async function addToken() {
  alert("addToken");
  const tokenAddress = '0xd00981105e61274c8a5cd5a88fe7e037d935b513';
  const tokenSymbol = 'DREX';
  const tokenDecimals = 18;
  const tokenImage = 'https://i.ibb.co/wspv1VN/drex.jpg';

  try {
    // wasAdded is a boolean. Like any RPC method, an error may be thrown.
    const wasAdded = await ethereum.request({
      method: 'wallet_watchAsset',
      params: {
        type: 'ERC20', // Initially only supports ERC20, but eventually more!
        options: {
          address: tokenAddress, // The address that the token is at.
          symbol: tokenSymbol, // A ticker symbol or shorthand, up to 5 chars.
          decimals: tokenDecimals, // The number of decimals in the token
          image: tokenImage, // A string url of the token logo
        },
      },
    });

    if (wasAdded) {
      alert('Cool!');
    } else {
      alert('Shame!');
    }
  } catch (error) {
    alert(error);
  }
}

document.getElementById("add-btn").addEventListener("click", addToken);